function Const = constants()

    global orbits;

    m = 4;       % Satellite mass [kg]
    lx = 0.1;    % X-axis length
    ly = 0.1;    % Y-axis length
    lz = 0.3405;    % Z-axis length

    Ix = (m / 12) * (ly^2 + lz^2); % Xaxis inertia
    Iy = (m / 12) * (lx^2 + lz^2); % Yaxis inertia
    Iz = (m / 12) * (lx^2 + ly^2); % Zaxis inertia

    I = diag([Ix Iy Iz]); % Inertia matrix
    I_inv = eye(3) / I;

    Re = 6371.2e3;                        % Earth radius [m]
    Rs = 400e3;                           % Satellite altitude [m]
    Radius = Re + Rs;                     % Distance from earth center to satellite [m]
    G = 6.67428e-11;                      % Earth gravitational constant
    M = 5.972e24;                         % Earth mass
    w_o = sqrt(G * M / Radius^3);         % Satellite angular velocity relative to Earth
    v_satellite = sqrt(G * M / Radius);   % Satellite's velocity in orbit
    w_o_io = [0 -w_o 0]';

    orbitPeriod = (2 * pi) / (w_o);       % Orbit Period
    n = orbitPeriod * orbits;             % Total simulation time

%%  Passing the values of the parameters in a struct.

    Const.Radius = Radius;
    Const.w_o = w_o;
    Const.v_satellite = v_satellite;
    Const.n = n;
    Const.orbitPeriod = orbitPeriod;
    Const.I = I;
    Const.I_inv = I_inv;
    Const.w_o_io = w_o_io;

end
