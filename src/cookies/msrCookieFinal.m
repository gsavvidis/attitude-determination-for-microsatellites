function cookie = msrCookieFinal(magn_ref,sun_ref,eclipse)

    cookie = struct('magn_ref',magn_ref,'sun_ref', sun_ref,'eclipse',eclipse);
end