function  [torque] = PD(q_desired , q_eci_body, q_orbit_body , w_b_ib , eclipse)
    
    Kp_gain= 1e-03*diag([1 10 1]);                                                  % Calculate gain (wrt Markley-Crassidis)
    Kd_gain= 1e-02*diag([1 10 1]);

    w_o_io = [0;-0.00113308802079241;0];
    if q_orbit_body(1)<0
        q_orbit_body = -q_orbit_body;
    end
    
  %  R_OB = quat2dcm(q_orbit_body'); % Calculating the transformation matrix from orbit to body frame

     q_w_b_io = quatProd(quatconj(q_orbit_body') ,quatProd([0;w_o_io],q_orbit_body));
     w_b_io = q_w_b_io(2:4);
% 
   w_b_ob = w_b_ib - w_b_io; % Calculating angular rate of satellite relative to ECI frame
%     w_b_ob=w_b_ib;
   q_error=quatProd(conj(q_desired),q_eci_body);

   torque = -sign(q_error(1))*Kp_gain*q_error(2:4) - Kd_gain*w_b_ob;
   
%    torque=T_commanded;
end